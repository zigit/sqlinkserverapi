﻿using Microsoft.AspNetCore.Mvc;
using SQLinkServer.Data;
using SQLinkServer.Models;
using SQLinkServer.Models.TimeReport;
using SQLinkServerApi.Logic;
using SQLinkServerApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using SQLinkServerApi.Models;

namespace SQLinkServerApi.Logic
{
    public class ReportsManager
    {
        readonly DataContext _dataContext;

        public ReportsManager(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public List<Reports> GetBillingLines()
        {
            List<InvoiceQueue> invoiceQueueList = _dataContext.InvoiceQueue.Include("BillingBatch").OrderByDescending(m => m.batchId).ToList();
            if (invoiceQueueList.Count == 0)
            {
                return null;
            }
            List<Reports> reportsList = new List<Reports>();
            reportsList.Add(new Reports() {
                managerName = invoiceQueueList[0].BillingBatch.managerName,
                date = invoiceQueueList[0].BillingBatch.date.ToString("dd/MM/yyyy"),
                totalInvoices = 1,
                successInvoices = invoiceQueueList[0].statusId == (int)InvoiceQueueStatus.success? 1:0,
                failedInvoices = invoiceQueueList[0].statusId == (int)InvoiceQueueStatus.error ? 1 : 0,
                totalAmount = invoiceQueueList[0].BillingBatch.total,
                batchId = invoiceQueueList[0].batchId,
            });
            for (int i = 1; i < invoiceQueueList.Count; i++)
            {
                if (invoiceQueueList[i].batchId != reportsList[(reportsList.Count - 1)].batchId)
                {
                    reportsList.Add(new Reports()
                    {
                        managerName = invoiceQueueList[i].BillingBatch.managerName,
                        date = invoiceQueueList[i].BillingBatch.date.ToString("dd/MM/yyyy"),
                        totalInvoices = 1,
                        successInvoices = invoiceQueueList[i].statusId == (int)InvoiceQueueStatus.success ? 1 : 0,
                        failedInvoices = invoiceQueueList[i].statusId == (int)InvoiceQueueStatus.error ? 1 : 0,
                        totalAmount = invoiceQueueList[i].BillingBatch.total,
                        batchId = invoiceQueueList[i].batchId,
                    });
                }
                else
                {
                    reportsList[(reportsList.Count - 1)].totalInvoices ++;
                    reportsList[(reportsList.Count - 1)].successInvoices += invoiceQueueList[i].statusId == (int)InvoiceQueueStatus.success ? 1 : 0;
                    reportsList[(reportsList.Count - 1)].failedInvoices += invoiceQueueList[i].statusId == (int)InvoiceQueueStatus.error ? 1 : 0;
                }
            }
            return reportsList;
        }
    }
}

﻿using SQLinkServer.Data;
using SQLinkServer.Models;
using SQLinkServer.Models.TimeReport;
using SQLinkServerApi.Logic;
using SQLinkServerApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SQLinkServer.Logic
{
    public class TimeReportsManager
    {
        readonly DIAPIManager _dIAPIManager;
        readonly InvoiceQueueManager _invoiceQueueManager;
        readonly AcountManager _acountManager;
        private readonly DataContext _dataContext;

        public TimeReportsManager(DIAPIManager dIAPIManager, DataContext dataContext, InvoiceQueueManager invoiceQueueManager, AcountManager acountManager)
        {
            _dataContext = dataContext;
            _dIAPIManager = dIAPIManager;
            _invoiceQueueManager = invoiceQueueManager;
            _acountManager = acountManager;
        }

        public List<ReturnTimeReport> GetAllTimeReportsLines(int month, int year, string name, int? salePersonCode)
        {
            int slpCode;
            if (salePersonCode == null)
            {
                slpCode = _dIAPIManager.GetSapUserId(name).Result;
            }
            else
            {
                slpCode = salePersonCode.Value;
            }
            List<int> listToDelete = new List<int>();
            List<TimeReport> listOfTimeReports = _dataContext.TimeReport.OrderByDescending(db => db.id).ToList();
            List<InvoicesExistFortMonthAndYear> InvoicesExist = _dIAPIManager.ReturnInvoicesExistFortMonthAndYear(month, year);
            List<Invoice> listOfInvoices = _dataContext.Invoice.OrderByDescending(db => db.Id).ToList();
            List<ReturnTimeReport> list = _dIAPIManager.GetOrdersLinesForTimeReport(slpCode);
            for (int i = 0; i < list.Count; i++)
            {
                TimeReport timeReport = listOfTimeReports.Where(db => db.orderLineId == list[i].orderLineId && db.orderId == list[i].orderId && db.month == month && db.year == year).FirstOrDefault();//GetTimeReportsLine(list[i].orderId, list[i].orderLineId, month, year);
                if (timeReport != null)
                {
                    //line is open and have been reported for current month
                    //checing for invoice   && db.Status.name == "success"
                    InvoiceQueue invoiceQueue = _invoiceQueueManager.GetInvoiceQueueLine(list[i].orderId, list[i].orderLineId, month, year);
                    if (invoiceQueue != null && invoiceQueue.statusId == (int)InvoiceQueueStatus.success)
                    {
                        if (invoiceQueue.doctype == (int)Doctypes.Invoicing && invoiceQueue.invoiceId != null)
                        {
                            Invoice invoice = listOfInvoices.Where(db => db.Id == invoiceQueue.invoiceId.Value).FirstOrDefault();
                            list[i].incoiceId = invoice.sapDocNum;
                        }
                        //line has invoice for current month editable = false
                        list[i].quantity = timeReport.quantity;
                        list[i].precent = timeReport.precent.Value;
                        list[i].editable = false;
                    }
                    else
                    {
                        //bool isInvoiceExistFortMonthAndYear = _dIAPIManager.isInvoiceExistFortMonthAndYear(list[i].orderId, list[i].orderLineId, month, year);
                        var isInvoiceExistFortMonthAndYear = InvoicesExist.Where(db => db.orderLineId == list[i].orderLineId && db.orderId == list[i].orderId && db.month == month && db.year == year).FirstOrDefault();
                        if (isInvoiceExistFortMonthAndYear != null)
                        {
                            listToDelete.Add(i);
                            continue;
                        }
                        //line is open, it has already been reported but invoice not created editable = true 
                        if (list[i].lineStatus.Equals("O"))
                        {
                            list[i].quantity = timeReport.quantity;
                            list[i].precent = timeReport.precent.Value;
                            list[i].editable = true;
                        }
                        else
                        {
                            listToDelete.Add(i);
                        }
                    }
                }
                else
                {
                    //line is open and never been reported for current month. 0 for open ,1 for close
                    if (list[i].lineStatus.Equals("O"))
                    {
                        list[i].quantity = 0;
                        list[i].editable = true;
                    }
                    else
                    {
                        listToDelete.Add(i);
                    }
                    //else line is cloded and never been reported for current month
                }
            }
            int deletedCounter = 0;
            for (int i = 0; i < listToDelete.Count; i++)
            {
                list.RemoveAt(listToDelete[i] - deletedCounter);
                deletedCounter++;
            }
            return list.OrderBy(m => m.customerCode).ThenBy(m => m.orderId).ThenBy(m => m.orderLineId).ToList();
        }

        public bool CheckLastMonthQuantity(TimeReport timeReport)
        {
            var taxDate = new DateTime(timeReport.year, timeReport.month, 1);
            taxDate = taxDate.AddMonths(-1);
            TimeReport lastMonthTimeReport =  _dataContext.TimeReport.Where(db => db.orderLineId == timeReport.orderLineId && db.orderId == timeReport.orderId && db.month == taxDate.Month && db.year == taxDate.Year).FirstOrDefault();
            if (lastMonthTimeReport!=null && lastMonthTimeReport.quantity * 1.5 <= timeReport.quantity)
            {
                return true;
            }
            return false;
        }

        public TimeReport GetTimeReportsLine(int orderId, int orderLineId, int month, int year)
        {
            var a = _dataContext.Users.ToList();
            return _dataContext.TimeReport.Where(db => db.orderLineId == orderLineId &&
               db.orderId == orderId && db.month == month && db.year == year).FirstOrDefault();
        }

        public List<SalePerson> GetAllSalesPersons(string userName)
        {
            int userSapId = _dIAPIManager.GetSapUserId(userName).Result;
            List<SalePerson> salePersons = _dIAPIManager.GetSalesPersons().Result;
            int? oldIndex = null;
            for (int i = 0; i < salePersons.Count; i++)
            {
                if (salePersons[i].slpCode == userSapId)
                {
                    oldIndex = i;
                }
            }
            if (oldIndex != null)
            {
                SalePerson item = salePersons[oldIndex.Value];
                salePersons.RemoveAt(oldIndex.Value);
                salePersons.Insert(0, item);
            }
            return salePersons;
        }

        public async Task UpsertTimeReportLine(UpsertTimeReport upsertTimeReport, string userName)
        {
            try
            {
                TimeReport existingTimeReport = _dataContext.TimeReport.Where(db => db.orderLineId == upsertTimeReport.orderLineId &&
                db.orderId == upsertTimeReport.orderId && db.month == upsertTimeReport.month && db.year == upsertTimeReport.year).FirstOrDefault();
                if (existingTimeReport == null)
                {
                    string userId = await _acountManager.GetUserID(userName);
                    TimeReport timeReport = new TimeReport(upsertTimeReport);
                    timeReport.userId = userId;
                    _dataContext.TimeReport.Add(timeReport);
                }
                else
                {
                    existingTimeReport.precent = upsertTimeReport.precent;
                    existingTimeReport.quantity = upsertTimeReport.quantity;
                    _dataContext.TimeReport.Update(existingTimeReport);
                }
                _dataContext.SaveChanges();
            }
            catch (Exception e)
            {

            }
        }
    }
}

﻿

using SQLinkServer.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;


namespace SQLinkServer.Logic
{
    public class AcountManager
    {
        private readonly IConfiguration _config;
        public readonly DIAPIManager _dIAPIManager;
        private readonly SignInManager<ApplicationUser> _signManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<Role> _roleManager;
        public AcountManager(IConfiguration config, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signManager, RoleManager<Role> roleManager, DIAPIManager dIAPIManager)
        {
            _config = config;
            _dIAPIManager = dIAPIManager;
            _signManager = signManager;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        private string BuildToken(ApplicationUser user)
        {
            var claims = new[]
            {
             new Claim(ClaimTypes.NameIdentifier, user.Id),
             new Claim(ClaimTypes.Name, user.UserName),
             new Claim(ClaimTypes.Role,"admin")};

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var tokenHandler = new JwtSecurityTokenHandler();
            var token = new JwtSecurityToken(null, null, claims, expires: DateTime.Now.AddDays(30), signingCredentials: creds);

            return tokenHandler.WriteToken(token);
        }

        private class IdentityUserModel
        {
            public string Name { get; set; }
            public string Email { get; set; }
            public DateTime Birthdate { get; set; }
        }

        public async Task<string> Authenticate(LoginModel login)
        {
            //No user in sap login
            bool isLoginToSapSucceed = _dIAPIManager.LoginToSap(login);
           
            if (!isLoginToSapSucceed)
            {
                //no user in sap  = no user at all
                return null;
            }
            else
            {
                //checks our db after sap login success
                ApplicationUser user = null;
                try
                {
                    user = await _userManager.FindByNameAsync(login.name);
                }
                catch (Exception e)
                {
                    throw;
                }
                if (user == null)
                {
                    user = await CreateUser(login);
                }
                else
                {
                    var res = await _signManager.CheckPasswordSignInAsync(user, login.password, false);
                    if (!res.Succeeded)
                    {
                        user = await CreateUser(login);
                    }
                }
                if (user == null)
                {
                    //problem in creating user
                    return null;
                }
                _dIAPIManager.SaveCompanyByKey(user.UserName);
                return BuildToken(user);
            }
        }

        public async Task<string> GetUserID(string name)
        {
           var user = await _userManager.FindByNameAsync(name);
           return user.Id;
        }
        public async Task<ApplicationUser> CreateUser(LoginModel login)
        {
            var user = new ApplicationUser { UserName = login.name };
            user.sapInvoiceId = _dIAPIManager.GetSapUserId(login.name).Result;
            var result = await _userManager.CreateAsync(user, login.password);
            if (result.Succeeded)
            {
                return user;
            }
            else
            {
                return null;
            }
        }

        public async Task<LoginModel> GetSessionUser(string name)
        {
            string companyKey = name;
            ApplicationUser user = await _userManager.FindByNameAsync(companyKey);
            return new LoginModel() {name = "נאווה", password = "1234" };
        }


    }
}

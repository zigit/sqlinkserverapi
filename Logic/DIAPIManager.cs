﻿
using SAPbobsCOM;
using SQLinkServer.Models;
using SQLinkServer.Models.TimeReport;
using SQLinkServerApi.Helpers;
using SQLinkServerApi.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Identity;
using System.Text;
using System.Data.SqlClient;

namespace SQLinkServer.Logic
{
    public class DIAPIManager
    {

        readonly SessonService _sessonService;
        private Company company;
        private IConfiguration _configuration;
        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<Company> GetCompany()
        {
            if (company == null)
            {
                await GetSessionUser(_sessonService.GetSessionUserCompanyKey());
            }

            return company;
        }

        public DIAPIManager(SessonService sessonService, IConfiguration configuration, UserManager<ApplicationUser> userManager)
        {
            _sessonService = sessonService;
            _configuration = configuration;
            _userManager = userManager;
            //'Set the connection context information to the DI API.  
            company = sessonService.GetCompany();
            if (company == null)
            {
                //error - no company for authorize user;
                //ReconnectToSap;
                //GetSessionUser(_sessonService.GetSessionUserCompanyKey());
            }
        }

        public async Task<LoginModel> GetSessionUser(string name)
        {
            string companyKey = name;
            ApplicationUser user = await _userManager.FindByNameAsync(companyKey);
            LoginToSap(new LoginModel() { name = "נאווה", password = "1234" });
            return new LoginModel() { name = "נאווה", password = "1234" };
        }

        public async Task<List<SalePerson>> GetSalesPersons()
        {
            List<SalePerson> list = new List<SalePerson>();
            Company com = await GetCompany();
            SAPbobsCOM.Recordset recordSet = com.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
            //Select all lines not globaly and 'O' for open
            //'C' for close LineStatus like 'O' 
            recordSet.DoQuery(
                $@"Select SlpCode,SlpName from OSLP where Active = 'Y'");

            for (int i = 0; i < recordSet.RecordCount; i++)
            {
                list.Add(new SalePerson(
                    (int)recordSet.Fields.Item("SlpCode").Value,
                    recordSet.Fields.Item("SlpName").Value.ToString()
                    ));
                recordSet.MoveNext();
            }
            return list;
        }

        public List<ReturnTimeReport> GetOrdersLinesForTimeReport(int code)
        {
            StringBuilder query = new StringBuilder();
            List<ReturnTimeReport> list = new List<ReturnTimeReport>();
            //SAPbobsCOM.Recordset recordSet = company.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;

            query.Append("Select ORDR.CardCode as ORDRCardCode,ORDR.CardName as ORDRCardName,ORDR.DocNum as  ORDRDocNum, ");
            query.Append("RDR1.LineNum, RDR1.ItemCode, RDR1.Dscription, [@ZIG_CONTRACTS].Name, RDR1.U_Zig_ContractType, RDR1.OpenQty, RDR1.Quantity, RDR1.DiscPrcnt, RDR1.LineStatus from RDR1 ");
            query.Append("INNER JOIN [@ZIG_CONTRACTS] ");
            query.Append("ON RDR1.U_Zig_ContractType=[@ZIG_CONTRACTS].Code ");
            query.Append("INNER JOIN ORDR ");
            query.Append("ON RDR1.DocEntry=ORDR.DocEntry ");
            query.Append("INNER JOIN OCRD ");
            query.Append("ON OCRD.CardCode = ORDR.CardCode ");
            query.Append("Where [@ZIG_CONTRACTS].Code != 1 ");
            query.Append($"and  (( OCRD.SlpCode = {code}) or (ORDR.SlpCode = {code}  and OCRD.SlpCode =-1))");

            //SQLINK_DOMAIN\zigit
            string connetionString = _configuration["SapConnectionString:SQLinkDb"];//"Data Source=192.168.2.145;initial catalog=zigit_testing25032019;persist security info=True; User ID=Zig1;Password = Zigit1";
            try
            {
                using (SqlConnection cnn = new SqlConnection(connetionString))
                {
                    using (SqlCommand command = new SqlCommand(query.ToString(), cnn))
                    {
                        cnn.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //TODO IN c'tor
                                ReturnTimeReport returnTimeReport = new ReturnTimeReport();
                                returnTimeReport.userCode = reader["ORDRCardCode"].ToString();
                                returnTimeReport.userName = reader["ORDRCardName"].ToString();
                                returnTimeReport.orderId = int.Parse(reader["ORDRDocNum"].ToString());
                                returnTimeReport.orderLineId = int.Parse(reader["LineNum"].ToString());
                                returnTimeReport.serviceCode = reader["ItemCode"].ToString();
                                returnTimeReport.description = reader["Dscription"].ToString();
                                returnTimeReport.contractType = reader["Name"].ToString();
                                returnTimeReport.contractTypeId = int.Parse(reader["U_Zig_ContractType"].ToString());
                                returnTimeReport.openQty = double.Parse(reader["OpenQty"].ToString());
                                returnTimeReport.quantity = double.Parse(reader["Quantity"].ToString());
                                returnTimeReport.precent = double.Parse(reader["DiscPrcnt"].ToString());
                                returnTimeReport.lineStatus = reader["LineStatus"].ToString();
                                list.Add(returnTimeReport);
                            }
                            reader.Close();
                            cnn.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return list;
        }

        public Dictionary<string, double> GetCurrencyForBilling()
        {
            Dictionary<string, double> currencys = new Dictionary<string, double>();
            SAPbobsCOM.Recordset recordSet = company.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
            recordSet.DoQuery(
               $@"Select * from ORTT
                where convert(varchar(10), [RateDate], 102) = convert(varchar(10), getdate(), 102)");
            for (int i = 0; i < recordSet.RecordCount; i++)
            {
                currencys.Add((string)recordSet.Fields.Item("Currency").Value, (double)recordSet.Fields.Item("Rate").Value);
                recordSet.MoveNext();
            }
            return currencys;
        }

        public Dictionary<int, ReturnBillingORDR> GetOrdersLinesForBilling(int code, int month, int year)
        {
            StringBuilder query = new StringBuilder();
            query.Append("Select ORDR.NumAtCard, ORDR.CardCode as ORDRCardCode,ORDR.CardName as ORDRCardName,ORDR.DocNum as ORDRDocNum,RDR1.Currency as RDR1Currency,RDR1.VisOrder as RDR1VisOrder ");
            query.Append(", OACT.AcctCode,RDR1.Dscription,RDR1.LineNum,RDR1.U_Zig_SplitInvoice,[@ZIG_CONTRACTS].Name,RDR1.U_Zig_ContractType,RDR1.ItemCode,RDR1.Quantity,RDR1.DiscPrcnt,RDR1.Price");
            query.Append(", RDR1.TotalSumSy,OACT.AcctName");
            query.Append(" from RDR1  INNER JOIN [@ZIG_CONTRACTS]   ON RDR1.U_Zig_ContractType=[@ZIG_CONTRACTS].Code INNER JOIN ORDR ON RDR1.DocEntry=ORDR.DocEntry INNER JOIN OCRD ON OCRD.CardCode = ORDR.CardCode INNER JOIN OACT   ON OACT.AcctCode = RDR1.AcctCode");
            query.Append($" Where LineStatus like 'O' and  ( OCRD.SlpCode ={code}) or (ORDR.SlpCode = {code} and OCRD.SlpCode =-1) ");
            query.Append(" order by RDR1.VisOrder");

            var lastDayOfMonth = getLastDayOfMonth(month, year).Day;
            var taxDate = new DateTime(year, month, lastDayOfMonth);

            Dictionary<int, ReturnBillingORDR> orders = new Dictionary<int, ReturnBillingORDR>();

            //SQLINK_DOMAIN\zigit
            string connetionString = _configuration["SapConnectionString:SQLinkDb"];//"Data Source=192.168.2.145;initial catalog=zigit_testing25032019;persist security info=True; User ID=Zig1;Password = Zigit1";
            try
            {
                using (SqlConnection cnn = new SqlConnection(connetionString))
                {
                    using (SqlCommand command = new SqlCommand(query.ToString(), cnn))
                    {
                        cnn.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //build modal
                                //TODO IN c'tor
                                ReturnBillingRDR returnBillingRDR = new ReturnBillingRDR();
                                string acctCode = reader["AcctCode"].ToString();
                                string dsc = reader["Dscription"].ToString();
                                returnBillingRDR.orderId = int.Parse(reader["ORDRDocNum"].ToString());
                                returnBillingRDR.orderLineId = (int)reader["LineNum"];
                                returnBillingRDR.splitInvoice = int.Parse(reader["U_Zig_SplitInvoice"].ToString());
                                returnBillingRDR.contractType = reader["Name"].ToString();
                                returnBillingRDR.contractTypeId = int.Parse(reader["U_Zig_ContractType"].ToString());
                                returnBillingRDR.serviceCode = reader["ItemCode"].ToString();
                                returnBillingRDR.description = acctCode == "41100" ? "שעות עבודה " + month + "/" + year : dsc;
                                returnBillingRDR.quantity = double.Parse(reader["Quantity"].ToString());
                                returnBillingRDR.precent = double.Parse(reader["DiscPrcnt"].ToString());
                                returnBillingRDR.price = double.Parse(reader["Price"].ToString());
                                returnBillingRDR.total = double.Parse(reader["TotalSumSy"].ToString());
                                returnBillingRDR.acctCode = acctCode;
                                returnBillingRDR.acctName = reader["AcctName"].ToString();
                                returnBillingRDR.currency = reader["RDR1Currency"].ToString();

                                if (orders.TryGetValue(returnBillingRDR.orderId, out ReturnBillingORDR returnBillingORDR))
                                {
                                    returnBillingORDR.linesInOrder.Add(returnBillingRDR);
                                    //returnBillingORDR.numberOfLines = string.Format("");
                                    returnBillingORDR.total += returnBillingRDR.price;
                                }
                                else
                                {
                                    ReturnBillingORDR newReturnBillingORDR = new ReturnBillingORDR();
                                    newReturnBillingORDR.userCode = reader["ORDRCardCode"].ToString();
                                    newReturnBillingORDR.userName = reader["ORDRCardName"].ToString();
                                    newReturnBillingORDR.orderId = returnBillingRDR.orderId;
                                    newReturnBillingORDR.numAtCard = reader["NumAtCard"].ToString();
                                    newReturnBillingORDR.total = returnBillingRDR.price;
                                    orders.Add(returnBillingRDR.orderId, newReturnBillingORDR);
                                    newReturnBillingORDR.linesInOrder.Add(returnBillingRDR);
                                }
                            }
                            reader.Close();
                            cnn.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return orders;
        }

        public async Task<int> GetSapUserId(string name)
        {
            Company com = await GetCompany();
            SAPbobsCOM.Recordset recordSet = com.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
            recordSet.DoQuery(
                $@"Select U_Zig_User,SlpCode from OSLP
                    left join OUSR
                    on OUSR.USERID = OSLP.U_Zig_User
                    Where USER_CODE LIKE N'{name}'");
            int a = (int)recordSet.Fields.Item("SlpCode").Value;
            return a;
        }

        private Documents GetOrdersLines()
        {
            return company.GetBusinessObject(BoObjectTypes.oOrders) as Documents;
        }

        void GetOrderInvoice()
        {

        }

        public bool LoginToSap(LoginModel login)
        {
            try
            {
                company = new Company();
            }
            catch (Exception ee)
            {
                var d = ee;
            }

            // Set the mandatory properties for the connection to the database.
            // here I bring only 2 of the 5 mandatory fields.
            // To use a remote Db Server enter his name instead of the string "(local)"
            // This string is used to work on a DB installed on your local machine
            // the other mandatory fields are CompanyDB, UserName and Password
            // I am setting those fields in the ChooseCompany Form

            company.Server = "sqgsap";
            company.DbServerType = BoDataServerTypes.dst_MSSQL2012;
            company.UserName = login.name;//"זיגיט";
            company.Password = login.password;//"1234";
            company.CompanyDB = _configuration["SapCompany"];
            company.language = BoSuppLangs.ln_English;

            // Use Windows authentication for database server.
            // True for NT server authentication,
            // False for database server authentication.
            //oCompany.UseTrusted = true;


            //int ret = oCompany.SetSboLoginContext(a);
            int x = company.Connect();
            var e = company.GetLastErrorDescription();
            return x == 0;
        }

        internal void SaveCompanyByKey(string userName)
        {
            _sessonService.SetCompanyKey(userName, company);
        }

        public bool isInvoiceExistFortMonthAndYear(int orderId, int orderLineId, int month, int year)
        {
            return false;
            var lastDayOfMonth = getLastDayOfMonth(month, year).Day;
            var taxDate = new DateTime(year, month, lastDayOfMonth);

            SAPbobsCOM.Recordset orderRecordSet = company.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
            orderRecordSet.DoQuery($@"select * from ORDR where DocNum = " + orderId);
            orderRecordSet.MoveFirst();
            var orderDocEntry = (int)orderRecordSet.Fields.Item("DocEntry").Value;

            SAPbobsCOM.Recordset invoiceRecordSet = company.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
            invoiceRecordSet.DoQuery($@" select *  from  INV1  inner join OINV on INV1.DocEntry = OINV.DocEntry
  where INV1.BaseEntry ={orderDocEntry} and INV1.BaseLine = {orderLineId} and OINV.TaxDate = '{taxDate}'");
            var isInvoiceExistCurrentMonthAndYear = invoiceRecordSet.RecordCount > 0;
            return isInvoiceExistCurrentMonthAndYear;
        }

        public List<InvoicesExistFortMonthAndYear> ReturnInvoicesExistFortMonthAndYear(int month, int year)
        {
            var InvoicesExistFortMonthAndYearList = new List<InvoicesExistFortMonthAndYear>();
            var lastDayOfMonth = getLastDayOfMonth(month, year).Day;
            var taxDate = new DateTime(year, month, lastDayOfMonth);

            SAPbobsCOM.Recordset recordSet = company.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
            recordSet.DoQuery($@"select ORDR.DocNum as ORDRDocNum, ORDR.DocEntry as ORDRDocEntry,RDR1.LineNum as RDR1LineNum,OINV.TaxDate as OINVTaxDate from ORDR 	
                INNER JOIN RDR1
                ON RDR1.DocEntry=ORDR.DocEntry
	            INNER JOIN INV1 
                ON INV1.BaseEntry = ORDR.DocEntry
				INNER JOIN OINV 
                ON INV1.DocEntry = OINV.DocEntry and INV1.BaseLine = RDR1.LineNum
                where OINV.TaxDate = '{taxDate}'");
            recordSet.MoveFirst();
            for (int i = 0; i < recordSet.RecordCount; i++)
            {
                var invoicesExist = new InvoicesExistFortMonthAndYear();
                invoicesExist.orderId = (int)recordSet.Fields.Item("ORDRDocNum").Value;
                invoicesExist.orderLineId = (int)recordSet.Fields.Item("RDR1LineNum").Value;
                string datestr = recordSet.Fields.Item("OINVTaxDate").Value.ToString();
                DateTime date = DateTime.Parse(datestr);
                invoicesExist.month = date.Month;
                invoicesExist.year = date.Year;
                InvoicesExistFortMonthAndYearList.Add(invoicesExist);
                recordSet.MoveNext();
            }
            return InvoicesExistFortMonthAndYearList;
        }

        public InvoiceQueueGroupValidationResult ValidateInvoiceQueueGroup(GroupedInvoiceQueueLine group)
        {
            foreach (var line in group.Lines)
            {
                //var drf1DocEntry = (int)recordSet.Fields.Item("DocEntry").Value;

                if (isInvoiceExistFortMonthAndYear(group.OrderId, line.orderLineId, group.Month, group.Year))
                {
                    return new InvoiceQueueGroupValidationResult { IsValid = false, Error = $@"invoice already exist for line {line.orderLineId} in order {group.OrderId}, month - {group.Month}, year - {group.Year}  " };
                }

            }
            return new InvoiceQueueGroupValidationResult { IsValid = true, Error = null };
        }

        public List<CreateInvoiceResult> CreateInvoice(GroupedInvoiceQueueLine group)
        {
            List<CreateInvoiceResult> result = new List<CreateInvoiceResult>(); //group all lines by split invoice filed, month, year, orderid
            InvoiceQueueGroupValidationResult validationResult = ValidateInvoiceQueueGroup(group);//TODO check if group is valid
            if (validationResult.IsValid)
            {
                SAPbobsCOM.Recordset recordSet = company.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                recordSet.DoQuery($@"select * from ORDR where DocNum = " + group.OrderId);
                recordSet.MoveFirst();
                var docEntry = (int)recordSet.Fields.Item("DocEntry").Value;
                var res = CreateInvoiceForGroup(group, docEntry); // create invoice for the group
                foreach (var r in res)
                {
                    result.Add(r);
                }

            }
            else // group is not valid
            {
                foreach (var line in group.Lines)
                {
                    var currentGroupResult = new CreateInvoiceResult { InvoiceQueueId = line.id, Error = validationResult.Error, StatusId = (int)InvoiceQueueStatus.error };
                    result.Add(currentGroupResult);
                }


            }

            return result;

        }

        private DateTime GetInvoiceDocDate()
        {
            //if (DateTime.Today.Month == month && DateTime.Today.Year == year)
            //{
            DateTime res = DateTime.Now;
            var today = DateTime.Today.Day;
            if (today <= 23)
            {
                var lastMonth = DateTime.Now.AddMonths(-1).Month;
                var currentyear = DateTime.Now.AddMonths(-1).Year;
                var numOfDays = DateTime.DaysInMonth(currentyear, lastMonth);
                res = new DateTime(currentyear, lastMonth, numOfDays);
            }
            return res;
            //}
            //else
            //{

            //    var lastMonth = new DateTime(year, month, 1).AddMonths(-1).Month;
            //    var numOfDays = DateTime.DaysInMonth(year, lastMonth);
            //    return new DateTime(year, lastMonth, numOfDays);

            //}

        }

        public DateTime getLastDayOfMonth(int month, int year)
        {
            var numOfDays = DateTime.DaysInMonth(year, month);
            return new DateTime(year, month, numOfDays);
        }

        private List<CreateInvoiceResult> CreateInvoiceForGroup(GroupedInvoiceQueueLine groupedLines, int docEntry)
        {

            List<CreateInvoiceResult> result = new List<CreateInvoiceResult>();

            try
            {
                SAPbobsCOM.Documents order = company.GetBusinessObject(BoObjectTypes.oOrders) as SAPbobsCOM.Documents;
                order.GetByKey(docEntry);
                SAPbobsCOM.Documents oinvoice = groupedLines.doctype == (int)Doctypes.Invoice_layout ? ((SAPbobsCOM.Documents)(company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDrafts))) : ((SAPbobsCOM.Documents)(company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInvoices)));
                oinvoice.CardCode = order.CardCode;

                foreach (var line in groupedLines.Lines)
                {
                    SAPbobsCOM.Recordset Line = company.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                    Line.DoQuery($@"select * from RDR1 where DocEntry = " + docEntry + " and LineNum = " + line.orderLineId);
                    Line.MoveFirst();
                    var accounCode = (string)Line.Fields.Item("AcctCode").Value;
                    var unitPrice = (double)Line.Fields.Item("Price").Value;
                    var dscription = (string)Line.Fields.Item("Dscription").Value;
                    //order.Lines.SetCurrentLine(line.orderLineId);
                    oinvoice.Lines.BaseEntry = order.DocEntry;
                    oinvoice.Lines.BaseLine = line.orderLineId;
                    oinvoice.Lines.BaseType = System.Convert.ToInt32(SAPbobsCOM.BoObjectTypes.oOrders);
                    oinvoice.Lines.Quantity = line.quantity;
                    oinvoice.Lines.Currency = line.currency;
                    oinvoice.Lines.ItemDescription = accounCode == "41100" ? "שעות עבודה " + groupedLines.Month + "/" + groupedLines.Year : dscription;
                    if (line.precent != null && line.precent != 0) // "גלובלי מקזז"
                    {
                        oinvoice.Lines.Quantity = (double)line.precent;
                        //oinvoice.Lines.UnitPrice = ((double)line.precent) * unitPrice;
                    }
                    else
                    {
                        //oinvoice.Lines.UnitPrice = unitPrice;
                    }
                    oinvoice.Lines.UnitPrice = unitPrice;

                    oinvoice.Lines.Add();
                }
                oinvoice.DocObjectCode = BoObjectTypes.oInvoices;
                oinvoice.DocDate = GetInvoiceDocDate();
                oinvoice.TaxDate = getLastDayOfMonth(groupedLines.Month, groupedLines.Year);
                oinvoice.UserFields.Fields.Item("U_VAT").Value = GetInvoiceDocDate();
                oinvoice.UserFields.Fields.Item("U_Zig_CreatedBy").Value = company.UserName;
                oinvoice.UserFields.Fields.Item("U_Zig_CreatedByBill").Value = "1";//"כן";
                var lRetCode = oinvoice.Add();
                var err = company.GetLastErrorDescription();


                foreach (var line in groupedLines.Lines)
                {
                    var currentGroupResult = new CreateInvoiceResult
                    {
                        orderId = line.orderId,
                        InvoiceQueueId = line.id,
                        Error = lRetCode == 0 ? null : err,
                        StatusId = lRetCode == 0 ? (int)InvoiceQueueStatus.success : (int)InvoiceQueueStatus.error,
                        CreatedInvoice = lRetCode == 0 ? GetLastCreatedInvoice(groupedLines.doctype, docEntry, line.orderLineId) : null,
                        doctype = groupedLines.doctype == (int)Doctypes.Invoicing ? (int)Doctypes.Invoicing : (int)Doctypes.Invoice_layout
                    };
                    result.Add(currentGroupResult);
                }



                return result;

            }

            catch (Exception e)
            {
                var err = company.GetLastErrorDescription();
                foreach (var line in groupedLines.Lines)
                {
                    var currentGroupResult = new CreateInvoiceResult
                    {
                        orderId = line.orderId,
                        InvoiceQueueId = line.id,
                        Error = err + " " + e.Message,
                        StatusId = (int)InvoiceQueueStatus.error,
                        CreatedInvoice = null,
                        doctype = groupedLines.doctype == (int)Doctypes.Invoicing ? (int)Doctypes.Invoicing : (int)Doctypes.Invoice_layout
                    };
                    result.Add(currentGroupResult);
                }
                return result;
            }


        }

        public CreatedInvoice GetLastCreatedInvoice(int doctype, int docEntry, int orderLineid)
        {
            if (doctype == (int)Doctypes.Invoicing)
            {
                SAPbobsCOM.Recordset recordSet = company.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                SAPbobsCOM.Recordset InvocieRecordSet = company.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                recordSet.DoQuery($@"select * from RDR1 where DocEntry = " + docEntry + " and LineNum = " + orderLineid);
                recordSet.MoveFirst();
                var invoiceEntry = (int)recordSet.Fields.Item("TrgetEntry").Value;
                InvocieRecordSet.DoQuery($@"select * from OINV where DocEntry = " + invoiceEntry);
                InvocieRecordSet.MoveFirst();
                var invoiceDocNum = (int)InvocieRecordSet.Fields.Item("DocNum").Value;
                return new CreatedInvoice { sapInvoiceId = invoiceDocNum, date = DateTime.Now };
            }
            else
            {
                SAPbobsCOM.Recordset recordSet = company.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                recordSet.DoQuery($@"select * from DRF1 inner Join ODRF ON DRF1.DocEntry = ODRF.DocEntry where DRF1.BaseEntry = " + docEntry + " and DRF1.BaseLine = " + orderLineid + " and ODRF.DocStatus = 'O'");
                recordSet.MoveFirst();
                var drf1DocEntry = (int)recordSet.Fields.Item("DocEntry").Value;


                //SAPbobsCOM.Recordset recordSet2 = company.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                //recordSet2.DoQuery($@"select * from ODRF where DocEntry = " + drf1DocEntry);
                //recordSet2.MoveFirst();
                //var draftInvoiceDocNum = (int)recordSet2.Fields.Item("DocNum").Value;
                return new CreatedInvoice { sapInvoiceId = drf1DocEntry, date = DateTime.Now };
            }

        }
    }
    //public class CompanyOrders
    //{
    //    public Company company;
    //    public Documents orders;
    //}


    //recordSet.DoQuery(
    //            $@"	Select ORDR.CardCode as ORDRCardCode,ORDR.CardName as ORDRCardName,ORDR.DocNum as  ORDRDocNum,OINV.DocNum as  OINVDocNum,* from RDR1 
    //            INNER JOIN [@ZIG_CONTRACTS] 
    //            ON RDR1.U_Zig_ContractType=[@ZIG_CONTRACTS].Code
    //INNER JOIN ORDR
    //            ON RDR1.DocEntry=ORDR.DocEntry
    //LEFT JOIN OINV
    //            ON RDR1.TrgetEntry=OINV.DocEntry
    //            Where [@ZIG_CONTRACTS].Name not LIKE N'%גלובאלי' 
    //            and OINV.DocNum is null
    //            and RDR1.SlpCode = {code}");
}

public class InvoicesExistFortMonthAndYear
{
    public int orderId;
    public int orderLineId;
    public int month;
    public int year;
}





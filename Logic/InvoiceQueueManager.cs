﻿
using SQLinkServer.Data;
using SQLinkServer.Models;
using SQLinkServer.Models.TimeReport;
using System.Linq;

namespace SQLinkServerApi.Logic
{
    public class InvoiceQueueManager
    {
        private readonly DataContext _dataContext;

        public InvoiceQueueManager(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public InvoiceQueue GetInvoiceQueueLine(int orderId, int orderLineId, int month, int year)
        {
            InvoiceQueue invoiceQueue = _dataContext.InvoiceQueue.Where(db => db.orderLineId == orderLineId &&
             db.orderId == orderId && db.month == month && db.year == year).OrderByDescending(db=>db.id).FirstOrDefault();
            if (invoiceQueue!=null)
            {
                //invoiceQueue.Status = _dataContext.Status.SingleOrDefault(db => db.id == invoiceQueue.statusId);
            }
            return invoiceQueue;
        }
    }
}

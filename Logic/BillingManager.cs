﻿using Microsoft.AspNetCore.Mvc;
using SQLinkServer.Data;
using SQLinkServer.Models;
using SQLinkServer.Models.TimeReport;
using SQLinkServerApi.Logic;
using SQLinkServerApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using SQLinkServerApi.Models;
using System.Text;

namespace SQLinkServer.Logic
{
    public class BillingManager
    {
        readonly DIAPIManager _dIAPIManager;
        readonly InvoiceQueueManager _invoiceQueueManager;
        readonly AcountManager _acountManager;
        readonly DataContext _dataContext;
        readonly TimeReportsManager _timeReportsManager;
        private readonly IServiceScopeFactory _serviceScopeFactory;

        public BillingManager(DIAPIManager dIAPIManager, DataContext dataContext, InvoiceQueueManager invoiceQueueManager, AcountManager acountManager, TimeReportsManager timeReportsManager, IServiceScopeFactory serviceScopeFactory)
        {
            _dataContext = dataContext;
            _dIAPIManager = dIAPIManager;
            _invoiceQueueManager = invoiceQueueManager;
            _acountManager = acountManager;
            _serviceScopeFactory = serviceScopeFactory;
            _timeReportsManager = timeReportsManager;
        }

        public Dictionary<int, ReturnBillingORDR> GetBillingLines(int month, int year, string name, int? salePersonCode)
        {
            int slpCode;
            if (salePersonCode == null)
            {
                slpCode = _dIAPIManager.GetSapUserId(name).Result;
            }
            else
            {
                slpCode = salePersonCode.Value;
            }
            List<InvoiceQueue> listOfInvoiceQueue = _dataContext.InvoiceQueue.OrderByDescending(db => db.id).ToList();
            List<TimeReport> listOfTimeReports = _dataContext.TimeReport.OrderByDescending(db => db.id).ToList();
            List<InvoicesExistFortMonthAndYear> InvoicesExist = _dIAPIManager.ReturnInvoicesExistFortMonthAndYear(month,year);

            List<int> listToDelete = new List<int>();
            Dictionary <int,ReturnBillingORDR> orders = _dIAPIManager.GetOrdersLinesForBilling(slpCode,month,year);
            Dictionary<string, double> currencys = _dIAPIManager.GetCurrencyForBilling();

            foreach (KeyValuePair<int, ReturnBillingORDR> order in orders)
            {

                listToDelete.Clear();
                var list = order.Value.linesInOrder;
                //System.Diagnostics.Debug.WriteLine("inside for time start" + watch.ElapsedMilliseconds + " list.Count " + list.Count);
                for (int i = 0; i < list.Count; i++)
                {
                    //bool isInvoiceExistFortMonthAndYear = _dIAPIManager.isInvoiceExistFortMonthAndYear(list[i].orderId, list[i].orderLineId, month, year);
                    var isInvoiceExistFortMonthAndYear = InvoicesExist.Where(db => db.orderLineId == list[i].orderLineId && db.orderId == list[i].orderId && db.month == month && db.year == year).FirstOrDefault();
                    InvoiceQueue invoiceQueue = listOfInvoiceQueue.Where(db => db.orderLineId == list[i].orderLineId && db.orderId == list[i].orderId && db.month == month && db.year == year).FirstOrDefault();//_invoiceQueueManager.GetInvoiceQueueLine(list[i].orderId, list[i].orderLineId, month, year);
                    if (invoiceQueue != null && invoiceQueue.statusId == (int)InvoiceQueueStatus.success)
                    {
                        listToDelete.Add(i);
                    }
                    else if (isInvoiceExistFortMonthAndYear != null)
                    {
                        listToDelete.Add(i);
                    }
                    else {
                        double currencyRate;
                        if (currencys.TryGetValue(list[i].currency, out currencyRate))
                        {
                            list[i].currencyRate = currencyRate;
                        }
                        else
                        {
                            list[i].currencyRate = null;
                        }

                        if (list[i].contractTypeId == 1)//אם גלובאלי
                        {
                            list[i].quantity = 1;
                            list[i].total = list[i].price;
                            list[i].precent = null;
                        }
                        else
                        {
                            TimeReport timeReport = listOfTimeReports.Where(db => db.orderLineId == list[i].orderLineId && db.orderId == list[i].orderId && db.month == month && db.year == year).FirstOrDefault();//_timeReportsManager.GetTimeReportsLine(list[i].orderId, list[i].orderLineId, month, year);
                            if (timeReport != null)
                            {
                                if (list[i].contractTypeId == 2)//אם גלובאלי מקזז
                                {
                                    list[i].precent = timeReport.precent.Value;
                                    list[i].quantity = 1;
                                    list[i].total = list[i].price * (list[i].precent.Value == 0 ? 0 : list[i].precent.Value);
                                }
                                else if (list[i].contractTypeId == 3)//אם שעתי
                                {
                                    list[i].quantity = timeReport.quantity;
                                    list[i].precent = null;
                                    list[i].total = list[i].price * list[i].quantity;
                                }
                            }
                            else
                            {
                                listToDelete.Add(i);
                            }
                        }
                    }
                }

                int deletedCounter = 0;
                int totalLinesBeforeRemove = list.Count;
                for (int i = 0; i < listToDelete.Count; i++)
                {
                    list.RemoveAt(listToDelete[i] - deletedCounter);
                    deletedCounter++;
                }           
                order.Value.numberOfLines = string.Format("{0} מתוך {1}", list.Count, totalLinesBeforeRemove);
                order.Value.isCanShowFully = totalLinesBeforeRemove == list.Count;
                //System.Diagnostics.Debug.WriteLine("inside for time end" + watch.ElapsedMilliseconds);
            }
       
            List<int> removeOrders = new List<int>();
            foreach (KeyValuePair<int, ReturnBillingORDR> order in orders)
            {
                if (order.Value.linesInOrder.Count == 0)
                {
                    removeOrders.Add(order.Key);
                }
            }
            for (int i = 0; i < removeOrders.Count; i++)
            {
                orders.Remove(removeOrders[i]);
            }
            return orders;
        }

        public async Task<int> InsertIntoInvoiceQueue(List<InvoiceQueue> InvoiceQueues, string username,double total)
        {
            BillingBatch billingBatch = new BillingBatch();
            billingBatch.date = DateTime.Now;
            billingBatch.userId = await _acountManager.GetUserID(username);
            billingBatch.managerName = username;
            billingBatch.total = total;
            _dataContext.BillingBatch.Add(billingBatch);
            _dataContext.SaveChanges();
            for (int i = 0; i < InvoiceQueues.Count; i++)
            {
                    InvoiceQueues[i].invoiceId = null;
                    InvoiceQueues[i].batchId = billingBatch.id;
                    InvoiceQueues[i].statusId = (int)InvoiceQueueStatus.ready;/*enum*/
                    _dataContext.InvoiceQueue.Add(InvoiceQueues[i]);
            }
            _dataContext.SaveChanges();
             CreateInvoices(billingBatch.id);
            return billingBatch.id;
        }

        public async Task<ReturnStatusInvoices> CheckStatusByBatchId(int batchId)
        {
            List<InvoiceQueue> Invoices = _dataContext.InvoiceQueue.Where(db => db.batchId == batchId).ToList();
            var listOfInvoices = Invoices.GroupBy(c => new
            {
                c.month,
                c.year,
                c.splitInvoice,
                c.orderId,
                c.doctype
            }).Select(gcs => new GroupedInvoiceQueueLine() { doctype = gcs.Key.doctype, Month = gcs.Key.month, Year = gcs.Key.year, InvoiceSplit = gcs.Key.splitInvoice, OrderId = gcs.Key.orderId, Lines = gcs.ToList() }).ToList();

            int finishedCounter = 0;
            if (listOfInvoices.Count == 0)
            {
                //throw exception
                return new ReturnStatusInvoices() { value = -1, message = "אין חשבוניות"};
            }
            for (int i = 0; i < listOfInvoices.Count; i++)
            {
                List<InvoiceQueue> list = listOfInvoices[i].Lines.ToList();
                bool isAllNotReady = true;
                for (int j = 0; j < list.Count; j++)
                {
                    if (list[j].statusId == (int)InvoiceQueueStatus.ready)
                    {
                        isAllNotReady = false;
                        break;
                    }
                }
                if (isAllNotReady)
                {
                    finishedCounter++;
                }
            }
            float finishedValue = 0;
            if (listOfInvoices.Count != 0)
            {
                finishedValue = (float)(finishedCounter / (float)listOfInvoices.Count) * 100;
            }
            return new ReturnStatusInvoices() { value = finishedValue, message = string.Format("{0} חשבוניות מתוך {1} ", finishedCounter, listOfInvoices.Count) };
        }

        internal List<InvoiceQueue> GetSummaryLines(int batchId)
        {
            var list = _dataContext.InvoiceQueue.Where(db => db.batchId == batchId).ToList();
            var invoices = _dataContext.Invoice.ToList();

            Dictionary<string, InvoiceQueue> invoicesDictonary = new Dictionary<string, InvoiceQueue>(); 

            if (list.Count == 0)
            {
                //מנסה ליצור חשבונית שוב לאחר שנשכשלה בפעם הראשונה
                list.Add(new InvoiceQueue() { statusId = (int)InvoiceQueueStatus.error });
            }
            else
            {
                for (int i = 0; i < list.Count; i++)
                {
                    string key = $"{list[i].orderId},{list[i].splitInvoice}";
                    if (invoicesDictonary.TryGetValue(key, out InvoiceQueue value))
                    {
                        value.total += list[i].total;
                    }
                    else
                    {
                        if (list[i].doctype == (int)Doctypes.Invoicing && list[i].invoiceId != null)
                        {
                            Invoice invoice = invoices.Where(db => db.Id == list[i].invoiceId.Value).FirstOrDefault();
                            list[i].invoiceId = invoice.sapDocNum;
                        }
                        else
                        {
                            list[i].invoiceId = null;
                        }
                        invoicesDictonary.Add(key, list[i]);
                    }
                }
            }
            return invoicesDictonary.Values.ToList();
        }

        public async void CreateInvoices(int batchId)
        {
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                var db = scope.ServiceProvider.GetService<DataContext>();
                string lockId = Guid.NewGuid().ToString(); ;
                var invoiceQueueLines = await db.InvoiceQueue.Where(i => i.batchId == batchId && i.statusId != (int)InvoiceQueueStatus.success && i.lockId == null).ToListAsync();
                foreach (var ql in invoiceQueueLines) // set lockid to new guid foreach line that we going to create invoice 
                {
                    ql.lockId = lockId;
                }
                db.SaveChanges();

                var invoiceQueueLinesToCreate = await db.InvoiceQueue.Where(i => i.batchId == batchId && (i.statusId != (int)InvoiceQueueStatus.success) && i.lockId == lockId).ToListAsync(); // get  lines by the guid
                if (invoiceQueueLinesToCreate.Count > 0)
                {
                    var groupedLines = invoiceQueueLinesToCreate.GroupBy(c => new
                    {
                        c.month,
                        c.year,
                        c.splitInvoice,
                        c.orderId,
                        c.doctype


                    }).Select(gcs => new GroupedInvoiceQueueLine() { doctype = gcs.Key.doctype, Month = gcs.Key.month, Year = gcs.Key.year, InvoiceSplit = gcs.Key.splitInvoice, OrderId = gcs.Key.orderId, Lines = gcs.ToList() }).ToList();

                    foreach (GroupedInvoiceQueueLine group in groupedLines)
                    {
                        List<CreateInvoiceResult> results = _dIAPIManager.CreateInvoice(group);
                        foreach (var res in results)
                        {
                            int? invoiceId = null;
                            if (res.CreatedInvoice != null) //  invoice creation succeed
                            {
                                var invoiceFromDb = res.doctype == (int)Doctypes.Invoicing ?

                                db.Invoice.FirstOrDefault(m => m.sapDocNum == res.CreatedInvoice.sapInvoiceId) : db.Invoice.FirstOrDefault(m => m.sapDocEnrty == res.CreatedInvoice.sapInvoiceId);

                                if (invoiceFromDb == null) // invoice not created yet in local db
                                {
                                    var newInvoice = res.doctype == (int)Doctypes.Invoicing ?
                                        new Invoice { sapDocNum = res.CreatedInvoice.sapInvoiceId, orderId = res.orderId, date = DateTime.Now } : new Invoice { sapDocEnrty = res.CreatedInvoice.sapInvoiceId, orderId = res.orderId, date = DateTime.Now };
                                    db.Invoice.Add(newInvoice);
                                    db.SaveChanges();
                                    invoiceId = newInvoice.Id;

                                }
                                else
                                {
                                    invoiceId = invoiceFromDb.Id;// invoice already created in local db
                                }
                            }
                            else //invoice creation failed
                            {
                                invoiceId = null;
                            }
                            var invoiceQueueEntity = db.InvoiceQueue.First(m => m.id == res.InvoiceQueueId);
                            invoiceQueueEntity.statusId = res.StatusId;
                            invoiceQueueEntity.error = res.Error;
                            invoiceQueueEntity.lockId = null; //relese the lock from all records
                            invoiceQueueEntity.invoiceId = invoiceId;
                            db.SaveChanges();
                        }

                    }
                }

            }

        }
        }

        public class ReturnStatusInvoices
        {
            public float value;
            public string message;
        }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SQLinkServerApi.Migrations
{
    public partial class managerName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "managerName",
                schema: "ZIG",
                table: "InvoiceQueue",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "managerName",
                schema: "ZIG",
                table: "InvoiceQueue");
        }
    }
}

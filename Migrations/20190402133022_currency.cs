﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SQLinkServerApi.Migrations
{
    public partial class currency : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "currency",
                schema: "ZIG",
                table: "InvoiceQueue",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "currency",
                schema: "ZIG",
                table: "InvoiceQueue");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SQLinkServerApi.Migrations
{
    public partial class changeQuantetyToDouble2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "quantity",
                schema: "ZIG",
                table: "InvoiceQueue",
                nullable: false,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "quantity",
                schema: "ZIG",
                table: "InvoiceQueue",
                nullable: false,
                oldClrType: typeof(double));
        }
    }
}

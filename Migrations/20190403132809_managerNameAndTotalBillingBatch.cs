﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SQLinkServerApi.Migrations
{
    public partial class managerNameAndTotalBillingBatch : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "managerName",
                schema: "ZIG",
                table: "InvoiceQueue");

            migrationBuilder.AddColumn<string>(
                name: "managerName",
                schema: "ZIG",
                table: "BillingBatch",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "total",
                schema: "ZIG",
                table: "BillingBatch",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "managerName",
                schema: "ZIG",
                table: "BillingBatch");

            migrationBuilder.DropColumn(
                name: "total",
                schema: "ZIG",
                table: "BillingBatch");

            migrationBuilder.AddColumn<string>(
                name: "managerName",
                schema: "ZIG",
                table: "InvoiceQueue",
                nullable: true);
        }
    }
}

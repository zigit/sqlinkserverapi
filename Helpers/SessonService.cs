﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity.UI.V3.Pages.Account.Internal;
using SAPbobsCOM;
using SQLinkServer.Logic;
using System.Collections.Generic;

namespace SQLinkServerApi.Helpers
{
    public class SessonService
    {

        //static public readonly string COMPANY_KEY = "companyKey";
        private readonly IHttpContextAccessor _httpContext;

        Dictionary<string, Company> companys = new Dictionary<string, Company>();

        public SessonService(IHttpContextAccessor httpContext)
        {
            _httpContext = httpContext;
        }

        public string GetSessionUserCompanyKey()
        {
            return _httpContext.HttpContext.User.Identity.Name;
        }

        public Company GetCompany()
        {
            string companyKey = _httpContext.HttpContext.User.Identity.Name;
            if (companyKey == null)
            {
                //error - return unauthorize and go to login page;
                return null;
            }
            companys.TryGetValue(companyKey, out Company company);
            return company;
        }


        //public async void ReLogin()
        //{
        //    LoginModel loginModel = await _acountManager.GetSessionUser(GetSessionUserCompanyKey());
        //    LoginToSap(loginModel);
        //}

        public void SetCompanyKey(string companyKey, Company company)
        {
            companys[companyKey] = company;
        }
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SQLinkServer.Models
{
    public class Invoice
    {
        [Key]
        public int Id { get; set; }

        public int? sapDocNum { get; set; }

        public int? sapDocEnrty { get; set; }

        public DateTime date { get; set; }

        public int orderId { get; set; }

        public int orderLineId { get; set; }

        public int customerId { get; set; }

    }
}

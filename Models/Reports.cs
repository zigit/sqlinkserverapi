﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SQLinkServerApi.Models
{
    public class Reports
    {
        public string managerName { get; set; }
        public string date { get; set; }
        public int totalInvoices { get; set; }
        public int successInvoices { get; set; }
        public int failedInvoices { get; set; }
        public double totalAmount { get; set; }
        public int batchId { get; set; }
    }
}

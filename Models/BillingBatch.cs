﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SQLinkServer.Models
{
    public class BillingBatch
    {
        [Key]
        public int id { get; set; }

        public DateTime date { get; set; }

        public string userId { get; set; }

        public double total { get; set; }

        public string managerName { get; set; }

        [ForeignKey("userId")]
        public ApplicationUser User { get; set; }
    }
}

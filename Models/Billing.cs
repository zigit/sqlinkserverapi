﻿using SQLinkServer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SQLinkServerApi.Models
{
    public class GroupedInvoiceQueueLine
    {
        public int Month { get; set; }
        public int Year { get; set; }
        public int OrderId { get; set; }
        public int InvoiceSplit { get; set; }
        public IEnumerable<InvoiceQueue> Lines { get; set; }
        public int doctype { get; set; }
    }

    public class InvoiceQueueGroupValidationResult
    {
        public bool IsValid { get; set; }
        public string Error { get; set; }
    }

    public class CreateInvoiceResult
    {
        public int orderId { get; set; }
        public int InvoiceQueueId { get; set; }

        public CreatedInvoice CreatedInvoice { get; set; }

        public int StatusId { get; set; }

        public string Error { get; set; }
        public int doctype { get; set; }

    }

    public class CreatedInvoice
    {
        public int sapInvoiceId { get; set; }

        public DateTime date { get; set; }

        public int orderId { get; set; }

        public int orderLineId { get; set; }

        public int customerId { get; set; }
    }
    enum InvoiceQueueStatus
    {
        ready = 1,
        success = 2,
        error = 3
    }

}

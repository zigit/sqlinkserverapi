﻿
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SQLinkServer.Models
{
    public class InvoiceQueue
    {

        [Key]
        public int id { get; set; }

        public int? invoiceId { get; set; }

        [ForeignKey("invoiceId")]
        public Invoice Invoice { get; set; }

        public int orderId { get; set; }

        public int orderLineId { get; set; }

        public int month { get; set; }

        public int year { get; set; }

        public int batchId { get; set; }

        [ForeignKey("batchId")]
        public BillingBatch BillingBatch { get; set; }

        public int splitInvoice { get; set; }

        public double quantity { get; set; }

        public double? precent { get; set; }

        public int statusId { get; set; }

        public string error { get; set; }

        public int doctype { get; set; }

        public string lockId { get; set; }

        public double total { get; set; }

        public string customerId { get; set; }

        public string customerName { get; set; }

        public string currency { get; set; }
    }

    public enum Doctypes
    {
        Invoicing,
        Invoice_layout
    }
}

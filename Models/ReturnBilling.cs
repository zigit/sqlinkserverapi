﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SQLinkServerApi.Models
{
    public class ReturnBillingORDR
    {
        public ReturnBillingORDR()
        {
            linesInOrder = new List<ReturnBillingRDR>();
            total = 0;
        }
        public string userCode { get; set; }

        public string userName { get; set; }

        public int orderId { get; set; }

        public string numAtCard { get; set; }

        public string numberOfLines { get; set; }

        public bool isCanShowFully { get; set; }

        public double total { get; set; }

        public List<ReturnBillingRDR> linesInOrder;
    }

    public class ReturnBillingRDR
    {
        public string acctName;

        public int orderId { get; set; }

        public int orderLineId { get; set; }

        public int splitInvoice { get; set; }

        public string contractType { get; set; }
        public int contractTypeId { get; set; }

        public string serviceCode { get; set; }

        public string description { get; set; }

        public double quantity { get; set; }

        public double? precent { get; set; }

        public double price { get; set; }

        public double total { get; set; }

        public string acctCode { get; set; }

        public string currency { get; set; }

        public double? currencyRate { get; set; }

    }
}

﻿
using System.ComponentModel.DataAnnotations;

namespace SQLinkServer.Models
{
    public class Status
    {
        [Key]
        public int id { get; set; }

        public string name { get; set; }

    }
}

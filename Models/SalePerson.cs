﻿
namespace SQLinkServerApi.Models
{
    public class SalePerson
    {
        public SalePerson(int slpCode, string slpName)
        {
            this.slpCode = slpCode;
            this.slpName = slpName;
        }
        public int slpCode;
        public string slpName;
    }
}

﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SQLinkServer.Models.TimeReport
{
    public class TimeReport
    {
        public TimeReport() { }

        public TimeReport(UpsertTimeReport upsertTimeReport)
        {
            orderId = upsertTimeReport.orderId;
            orderLineId = upsertTimeReport.orderLineId;
            month = upsertTimeReport.month;
            year = upsertTimeReport.year;
            quantity = upsertTimeReport.quantity;
            precent = upsertTimeReport.precent;
            this.userId = userId;
        }

        [Key]
        public int id {get; set;}

        public int orderId { get; set; }

        public int orderLineId { get; set; }

        public int month { get; set; }

        public int year { get; set; }

        public string userId { get; set; }

        [ForeignKey("userId")]
        public ApplicationUser User { get; set; }

        public double? precent { get; set; }

        public double quantity { get; set; }

    }
}

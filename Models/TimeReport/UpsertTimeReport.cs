﻿
namespace SQLinkServer.Models.TimeReport
{
    public class UpsertTimeReport
    {
        public int orderId { get; set; }

        public int orderLineId { get; set; }

        public int month { get; set; }

        public int year { get; set; }

        public double precent { get; set; }

        public double quantity { get; set; }
    }
}

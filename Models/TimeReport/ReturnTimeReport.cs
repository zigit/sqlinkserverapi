﻿
namespace SQLinkServer.Models.TimeReport
{
    public class ReturnTimeReport
    {
        public ReturnTimeReport()
        {

        }

        public string userCode {get; set;}

        public string userName { get; set; }

        public int orderId { get; set; }

        public int orderLineId { get; set; }

        public string description { get; set; }

        public string serviceCode { get; set; }

        public string contractType { get; set; }

        public int contractTypeId { get; set; }

        public string customerCode { get; set; }

        public string customerType { get; set; }

        public double openQty { get; set; }

        public double quantity { get; set; }

        public double precent { get; set; }

        public int? incoiceId { get; set; }

        public bool editable { get; set; }
        
        public string[] warnings { get; set; }

        public string lineStatus { get; set; }

    }
}

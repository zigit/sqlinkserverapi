﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.AspNetCore.Mvc;
using SQLinkServer.Models;
using SQLinkServer.Models.TimeReport;
using Microsoft.AspNetCore.Http;
using SQLinkServer.Logic;
using Microsoft.AspNetCore.Authorization;
using SQLinkServerApi.Models;
using System.Threading.Tasks;

namespace SQLinkServer.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/TimeReports/[action]")]
    public class TimeReportsController : Controller
    {
        public readonly TimeReportsManager _timeReportsManager;

        public TimeReportsController(TimeReportsManager timeReportsManager)
        {
            _timeReportsManager = timeReportsManager;
        }
        // GET: api/TimeReports/GetTimeReportsLines
        [HttpGet("{month}/{year}/{salePersonCode?}")]
        public IEnumerable<ReturnTimeReport> GetTimeReportsLines([FromRoute] int month, [FromRoute] int year, [FromRoute] int? salePersonCode)
        {
            return _timeReportsManager.GetAllTimeReportsLines(month,year, HttpContext.User.Identity.Name, salePersonCode);
        }

        // GET: api/TimeReports/GetAllSalesPersons
        [HttpGet]
        public IEnumerable<SalePerson> GetAllSalesPersons()
        {
            var userName = HttpContext.User.Identity.Name;
            return _timeReportsManager.GetAllSalesPersons(userName);
        }

        [HttpPost]
        public bool CheckLastMonthQuantity([FromBody] TimeReport timeReport)
        {
            return _timeReportsManager.CheckLastMonthQuantity(timeReport);
        }

        [HttpPut]
        public async Task UpsertTimeReportLine([FromBody] UpsertTimeReport UpsertTimeReport)
        {
            var userName = HttpContext.User.Identity.Name;
           await  _timeReportsManager.UpsertTimeReportLine(UpsertTimeReport, userName);
        //    return null;//GetTimeReportsLines(UpsertTimeReport.month, UpsertTimeReport.year);
        }
    }
}

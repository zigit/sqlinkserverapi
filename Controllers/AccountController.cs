using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using SQLinkServer.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SQLinkServerApi.Helpers;
using SQLinkServer.Models;
using SQLinkServer.Logic;
using Newtonsoft.Json;

namespace SQLinkServer.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly AcountManager acountManager;
        private IConfiguration _config;

        public AccountController(IConfiguration config, AcountManager acountManager)
        {
            this.acountManager = acountManager;
            _config = config;
        }


        //[AllowAnonymous]
        //[HttpPost]
        //[Route("Register")]
        //public async Task<IActionResult> Register([FromBody]LoginModel model)
        //{
        //    var user = new IdUser { UserName = model.userName };

        //    var result = await _userManager.CreateAsync(user, model.password);
        //    if (result.Succeeded)
        //    {
        //        return Ok("Success");
        //    }
        //    else
        //    {
        //        return BadRequest();
        //    }
        //}

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Login([FromBody]LoginModel login)
        {
            try
            {
                IActionResult response = Unauthorized();
                string token = await acountManager.Authenticate(login);

                if (token != null)
                {
                    //sessonService.SetCompanyKey(loginData.GetOCompany());
                    //HttpContext.Session.Set("_isConnected", ByteConvertor.ObjectToByteArray(loginData.GetOCompany()));
                    response = Ok(new { token = token });
                }

                return response;
            }
            catch(Exception e)
            {
                return Ok(e.ToString());
            }
           
        }
    }
}

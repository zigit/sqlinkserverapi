﻿
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using SQLinkServer.Logic;
using System.Collections.Generic;
using SQLinkServer.Models.TimeReport;
using System.Threading.Tasks;
using SQLinkServerApi.Models;
using SQLinkServer.Models;
using System;
using System.Threading.Tasks;

namespace SQLinkServer.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Billing/[action]")]
    public class BillingController : Controller
    {
        public readonly BillingManager _billingManager;

        public BillingController(BillingManager billingManager)
        {
            _billingManager = billingManager;
        }

        // GET: api/TimeReports/GetTimeReportsLines/{month}/{year}/{salePersonCode?}
        [HttpGet("{month}/{year}/{salePersonCode?}")]
        public Dictionary<int, ReturnBillingORDR> GetBillingLines([FromRoute] int month, [FromRoute] int year, [FromRoute] int? salePersonCode)
        {
            return _billingManager.GetBillingLines(month, year, HttpContext.User.Identity.Name, salePersonCode);
        }

        //PrepareInvoices
        // POST: api/Billing
        [HttpPost("{total}")]
        public async Task<int> InsertIntoInvoiceQueue([FromBody]List<InvoiceQueue> InvoiceQueues, [FromRoute] double total)
        {
            var userName = HttpContext.User.Identity.Name;
            return await _billingManager.InsertIntoInvoiceQueue(InvoiceQueues, userName, total);
        }

        [HttpGet("{batchId}")]
        public async Task<ReturnStatusInvoices> CheckStatusByBatchId([FromRoute] int batchId)
        {
            return await _billingManager.CheckStatusByBatchId(batchId);
        }

        [HttpGet("{batchId}")]
        public List<InvoiceQueue> GetSummaryLines([FromRoute] int batchId)
        {
            return _billingManager.GetSummaryLines(batchId);
        }

        [HttpPost("{batchId}")]
        public async Task<IActionResult> CreateInvoices(int batchId)
        {
            _billingManager.CreateInvoices(batchId);
            return Ok("OK");
        }    
    }
}

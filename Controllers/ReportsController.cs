﻿
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using SQLinkServer.Logic;
using System.Collections.Generic;
using SQLinkServer.Models.TimeReport;
using System.Threading.Tasks;
using SQLinkServerApi.Models;
using SQLinkServer.Models;
using System;
using System.Threading.Tasks;
using SQLinkServerApi.Logic;

namespace SQLinkServerApi.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Reports/[action]")]
    public class ReportsController : Controller
    {
        public readonly ReportsManager _reportsManager;

        public ReportsController(ReportsManager reportsManager)
        {
            _reportsManager = reportsManager;
        }

        [HttpGet]
        public List<Reports> GetReportsLines()
        {
            return _reportsManager.GetBillingLines();
        }
    }
}

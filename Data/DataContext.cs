using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SQLinkServer.Models;
using SQLinkServer.Models.TimeReport;

namespace SQLinkServer.Data
{
    public class DataContext : IdentityDbContext<ApplicationUser, Role,string>
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
            // public DbSet<int> MyProperty { get; set; }
        }

        public DbSet<TimeReport> TimeReport { get; set; }
        public DbSet<Invoice> Invoice { get; set; }
        public DbSet<InvoiceQueue> InvoiceQueue { get; set; }
        //public DbSet<Status> Status { get; set; }
        public DbSet<BillingBatch> BillingBatch { get; set; }
        //public DbSet<User> User { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.HasDefaultSchema("ZIG");
            base.OnModelCreating(builder);
         
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
    }
}